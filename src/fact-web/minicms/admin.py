from .models import Block, Page
from django.contrib import admin
from django.contrib.admin.widgets import AdminTextareaWidget
from django.db import models
from tinymce.widgets import TinyMCE

# The default TextField doesn't have enough rows
class UsableTextarea(AdminTextareaWidget):
    def __init__(self, attrs=None):
        default_attrs = {'rows': '32'}
        if attrs:
            default_attrs.update(attrs)
        super(UsableTextarea, self).__init__(default_attrs)

class BaseAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': UsableTextarea},
    }

class HtmlAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': TinyMCE(attrs={'cols': 80, 'rows': 30,},
			mce_attrs={'theme': 'advanced',
			'theme_advanced_blockformats' : '"p,div,h1,h2,h3,h4,h5,h6,blockquote,dt,dd,code,samp',
			'theme_advanced_disable': 'styleselect',},)},
    }

class PageAdmin(HtmlAdmin):
    fields = ('url', 'title', 'content', 'show_share_buttons', 'published')
    list_display = ('url', 'title', 'show_share_buttons', 'published')
    search_fields = ('url',)
    ordering = ('url',)


class BlockAdmin(BaseAdmin):
    list_display = ('name',)
    search_fields = ('name',)
    ordering = ('name',)

admin.site.register(Page, PageAdmin)
admin.site.register(Block, BlockAdmin)
